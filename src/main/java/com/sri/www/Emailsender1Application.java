package com.sri.www;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

@SpringBootApplication
public class Emailsender1Application {

    @Autowired
    public EmailService emailservice; 

    public static void main(String[] args) {
        SpringApplication.run(Emailsender1Application.class, args);
    }

    @EventListener(ApplicationReadyEvent.class)
    public void sendEmail(){
    	
    	List<String> toEmails = Arrays.asList("srikanth.kovuri@halftonesystems.com","nadigottuswetha@gmail.com", "Sharonsanjana2000@gmail.com","npraveenkumar3747@gmail.com","vijaynatte144@gmail.com");
    	List<String> recipientName =Arrays.asList( "srikanth","swetha","Sharon","Praveen","vijay"); 
    	String subject = "Subject";
    	String body = "Happy Friday ";

    	emailservice.sendEmails(toEmails, subject, body, recipientName);

    }
}
