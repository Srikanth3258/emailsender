package com.sri.www;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailService {
    @Autowired
    private JavaMailSender mailsender;

    public void sendEmails(List<String> toEmails, String subject, String body,List< String> recipientNames) {
        LocalTime currentTime = LocalTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("hh:mm a");
        String formattedTime = currentTime.format(formatter);

        String greeting;
        if (currentTime.isBefore(LocalTime.NOON)) {
            greeting = "Good Morning";
        } else if (currentTime.isBefore(LocalTime.of(16, 0))) {
            greeting = "Good Afternoon";
        } else if (currentTime.isBefore(LocalTime.of(22, 0))) {
            greeting = "Good Evening";
        } else {
            greeting = "Good Night";
        }

        if (toEmails.size() != recipientNames.size()) {
            throw new IllegalArgumentException("Number of emails and recipient names should be the same.");
        }

        for (int i = 0; i < toEmails.size(); i++) {
            String toEmail = toEmails.get(i);
            String recipientName = recipientNames.get(i);

            String emailBody = greeting + ", " + recipientName + "!\n\n" + body + "\n\n" + "Current Time: " + formattedTime;

            SimpleMailMessage message = new SimpleMailMessage();
            message.setFrom("Srikanthchinni666@gmail.com");
            message.setTo(toEmail);
            message.setText(emailBody);
            message.setSubject(subject);
            mailsender.send(message);

            System.out.println("Email Sent to " + toEmail);
        }
    }
}
